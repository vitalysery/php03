<?php

class Filesystem {
    private $homedir;

    // Устанавливаем название домашней директории пользователя
    public function __construct($homedir) {
        $this->homedir = $homedir;
    }

    // Проверка существования домашнего каталога пользователя
    // Если каталога не существует, создаем его
    public function checkUserHomeDirectory()
    {
        if (!file_exists("users/" . $this->homedir . "/")) {
            mkdir("users/" . $this->homedir . "/", 700);
        }
    }

    // Метод для перевода размера файла в "человекочитаемый" вид
    public function bytesToSize($bytes, $precision = 2)
    {
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;

        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';

        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';

        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';

        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';

        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }

    // Метод возвращает массив со всеми файлами, которые содержатся в домашней дректории пользователя
    public function showFileTree()
    {
        // Инициализируем массив файлов
        $files_list = array();

        if ($handle = opendir('./users/' . $this->homedir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $files_list[] = array('name' => $entry, 'size' => filesize('./users/' . $this->homedir . '/' . $entry), 'size_to_display' => self::bytesToSize(filesize('./users/' . $this->homedir . '/' . $entry)));
                }
            }
            closedir($handle);
        }
        return $files_list;
    }

    // Удаление файла из домашней директории
    public function removeFile($filename) {
        if (unlink('./users/' . $this->homedir . '/' . $filename)) {
            return 1;
        } else {
            return 0;
        }
    }

    // Метод для получения размера файлов в директории
    // Параметр $extension необязательный
    // Если задан параметр $extension, метод вычисляет размер файлов только для заданного расширения
    function getDirsize($dir, $extension = false)
    {

        if (is_file($dir)) return array('size' => filesize($dir), 'howmany' => 0, 'size_to_display' => 0);

        if ($dh = opendir($dir)) {

            // Счетчик размера файлов
            $size = 0;

            // Счетчик количества файлов
            $n = 0;

            while (($file = readdir($dh)) !== false) {
                if ($extension === false) {
                    if ($file == '.' || $file == '..') continue;
                } else {
                    if ($file == '.' || $file == '..' || end(explode(".", $file)) != $extension) continue;
                }

                $n++;
                $data = $this->getDirsize($dir . '/' . $file);
                $size += $data['size'];
                $n += $data['howmany'];
            }
            closedir($dh);

            // Возвращаем массив со значениями
            // size: Размер файлов в байтах
            // howmany: Количество файлов
            // size_to_display: Отформатированный размер файлов для отображения в шаблоне
            return array('size' => $size, 'howmany' => $n, 'size_to_display' => self::bytesToSize($size));
        }
        return array('size' => 0, 'howmany' => 0);
    }

    function fileSaveIntoHomeDir($url)
    {
        $local_file = 'users/' . $this->homedir . '/' . end((explode("/", $url)));
        $remote_file = $url;

        // Инициализируем сеанс CURL
        $ch = curl_init();

        // Открываем файл для записи
        $fp = fopen($local_file, 'w') or die("Ошибка при открытии файла!");

        // Ассоциируем CURL с загружаемым файлом
        $ch = curl_init($remote_file);

        // Даем curl файл, в который сохраняем результат операции
        curl_setopt($ch, CURLOPT_FILE, $fp);

        // Разрешаем поддержку перенаправлений
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // Выполняем операцию
        curl_exec($ch);

        // Закрываем соединение
        curl_close($ch);

        // Закрываем файл
        fclose($fp);
    }


    function fileDownloadDirectly($url)
    {
        // Инициализируем сеанс CURL
        $ch = curl_init();

        // Запрещаем проверку сертификата удаленного сервера
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        // Разрешаем поддержку перенаправлений
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // Убираем лимит на время выполнения запроса
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);

        // Устанавливаем максимальное значение для времени подключения
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);

        // Выключаем индикацию прогресса
        curl_setopt($ch, CURLOPT_NOPROGRESS, 1);

        // Выключаем вывод сообщений о производимых действиях
        curl_setopt($ch, CURLOPT_VERBOSE, 0);

        // Отключаем возвращение результата операции
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);

        // Ассоциируем CURL с url
        curl_setopt($ch, CURLOPT_URL, $url);

        // Устанавливаем скачиваемому файлу такое же имя по умолчанию, как и его исходное имя на сервере
        $filename = end((explode("/", $url)));

        // Узнаем расширение файла для последующей установки content-type
        $file_extension = end((explode(".", $filename)));

        // Устанавливаем content-type на основе полученного расширения файла.
        // Если расширение не содержится в нашей таблице, то устанавливаем content-type по умолчанию
        switch ($file_extension) {
            case 'torrent':
                $mime_type = 'application/x-bittorrent';
            case '7z':
                $mime_type = 'application/x-7z-compressed';
            case 'swf':
                $mime_type = 'application/x-shockwave-flash';
            case 'pdf':
                $mime_type = 'application/pdf';
            case 'avi':
                $mime_type = 'video/x-msvideo';
            case 'bmp':
                $mime_type = 'image/bmp';
            case 'css':
                $mime_type = 'text/css';
            case 'curl':
                $mime_type = 'text/vnd.curl';
            case 'mp4':
                $mime_type = 'video/mp4';
            case 'jpg':
                $mime_type = 'image/jpeg';
            case 'jpeg':
                $mime_type = 'image/jpeg';
            default:
                $mime_type = 'application/octet-stream';
                break;
        }

        // Команда браузеру скачивать контент, а не открывать страницу
        header('Content-type: ' . $mime_type);
        header('Content-disposition: attachment; filename=' . $filename);

        // Выполняем операцию
        curl_exec($ch);
    }
}