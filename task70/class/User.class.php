<?php

class User
{
    // Имя пользователя
    private $login;

    // Пароль
    private $password;

    // Сеттер имени пользователя
    public function setUsername($username)
    {
        $this->login = $username;
    }

    // Сеттер пароля пользователя
    public function setPassword($password)
    {
        $this->password = $password;
    }

    // Геттер имени пользователя
    public function getUsername()
    {
        return $this->login;
    }

    // Геттер пароля пользователя
    public function getUserPassword()
    {
        return $this->password;
    }

    public function setRememberMeCookie($value)
    {
        if (!empty($this->login)) {
            if(!isset($_COOKIE['remember_me'])) {
                setcookie('remember_me', $value, time() + 3600);
            }
        }
    }


}