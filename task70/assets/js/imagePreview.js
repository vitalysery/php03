$(document).ready(function(){

    // При наведении курсора на имя файла
    $('.show-image').mouseenter(function(e){
        // Ищем следующий за ссылкой с именем файла контейнер для превью и "показываем" его
        $(e.target).next('.image_preview_block').css('display', 'block');
    }).mouseleave(function(e){
        // Скрываем контейнер для превью при "убирании" курсора с ссылки с названием файла
        $(e.target).next('.image_preview_block').css('display', 'none');
    });
});