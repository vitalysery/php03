$(document).ready(function () {
    deleteFile = function (element) {
        var trgt = $(element.target);

        // Создание объекта FormData
        var fd = new FormData();

        // Чтение буферного значения, которое сожержит [ID] пользователя
        var uid = $('#uid').val();
        // Добавление поля, которое содержит хеш имени файла
        fd.append("sha1", trgt.attr("data-sha1"));

        // Добавление поля, которое содержит идентификатор пользователя
        fd.append("uid", uid);

        $.ajax({
            url: 'xhr_file_delete.php',
            type: 'POST',
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (resp) {
                if (resp.message != '') {
                    if (resp.message == null || resp.messaage == 'undefined') {
                        $('#notification-wrap').html('<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>Ошибка во время удаления файла.</strong> </div> </div> </div>');
                    } else {
                        $('#notification-wrap').html('<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>' + resp.message + '</strong> </div> </div> </div>');
                    }

                    // Обновляем информацию о файловой системе и пользователях в соответствии с данными, полученными в ответ
                    $('#sum_of_user_sizes').html(resp.sum_of_user_sizes);
                    $('#user_storage_limit').html(resp.user_storage_limit);
                    $('#count_users').html(resp.count_users);
                    $('#count_files').html(resp.count_files);
                    $('#sum_of_sizes').html(resp.sum_of_sizes);
                    $('#avg_files_per_all_users').html(resp.avg_files_per_all_users);
                }
            }
        });


        // Поиск элементв tr, которому принадлежит кнопка на удаление файла
        // Задаем цвет фона
        trgt.parent().closest('tr').css('background', 'red');

        // Анимируем изменение цвета фона
        trgt.parent().closest('tr').animate({backgroundColor: "#fff"}, 1000);

        // Удаляем tr из таблицы с удаляемым файлом
        setTimeout(function () {
            trgt.closest('tr').remove();
        }, 1000);
    }

    $('#button_file_upload').click(function () {
        var file = $('#file_to_upload').prop('files')[0];
        var uid = $('#uid').val();

        // Создание объекта FormData
        var fd = new FormData();

        fd.append("file_to_upload", file);
        fd.append("uid", uid);

        if (fd) {
            $('#progressBar').css('display', 'block');

            $.ajax({
                url: 'xhr_file_upload.php',
                type: 'POST',
                data: fd,
                dataType: 'json',
                processData: false,
                contentType: false,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (e) {
                        if (e.lengthComputable) {
                            // Вычисление числа процентов, на которое загружен файл
                            var percentComplete = (e.loaded / e.total) * 100;

                            // Помещаем значение в progressbar
                            document.querySelector('#progressBar').value = Math.round(percentComplete);
                        }
                    }, false);
                    return xhr;
                },
                success: function (resp) {
                    $('#progressBar').css('display', 'none');

                    // Отображение сообщения для пользователя на экран
                    if (resp.message != '') {
                        if (resp.message == null || resp.messaage == 'undefined') {
                            $('#notification-wrap').html('<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>Ошибка во время загрузки файла.</strong> </div> </div> </div>');
                        } else {
                            $('#notification-wrap').html('<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>' + resp.message + '</strong> </div> </div> </div>');
                        }

                        $('#filelist:last').append('<tr><td><a href="./users/' + resp.user_homedir + '/' + resp.filename + '" download>' + resp.filename + '</a></td><td>' + resp.filesize + '</td><td>' + resp.timestamp + '</td><td><a href="./users/' + resp.user_homedir + '/' + resp.filename + '" download class="btn btn-default">Скачать</a> <a href="#" data-sha1="' + resp.sha1 + '" class="btn btn-danger delete-file">Удалить</a></td></tr>')

                        $('#filelist tr:last').addClass('green_background');
                        $('#filelist tr:last').animate({backgroundColor: "#fff"}, 1000);

                        // Обновляем информацию о файловой системе и пользователях в соответствии с данными, полученными в ответ
                        $('#sum_of_user_sizes').html(resp.sum_of_user_sizes);
                        $('#user_storage_limit').html(resp.user_storage_limit);
                        $('#count_users').html(resp.count_users);
                        $('#count_files').html(resp.count_files);
                        $('#sum_of_sizes').html(resp.sum_of_sizes);
                        $('#avg_files_per_all_users').html(resp.avg_files_per_all_users);

                        // Добавляем обработчик события на кнопки удаления файла
                        $(".delete-file").click(function (e) {
                            deleteFile(e);
                        });
                    }
                }
            });
        }
    });

    // Добавляем обработчик события на кнопки удаления файла
    $(".delete-file").click(function (e) {
        deleteFile(e);
    });
});
