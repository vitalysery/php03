// Управление кнопками и блоками "Загрузить файл с жесткого диска" и "Загрузить файл по URL"
$(document).ready(function () {
    $('#add_file_button').click(function () {
        $('#add_file_form').toggle();
        $('#add_file_from_url_form').hide();
    });

    $('#cancel_button').click(function () {
        $('#add_file_form').hide();
    });

    $('#add_file_from_url_button').click(function () {
        $('#add_file_from_url_form').toggle();
        $('#add_file_form').hide();
    });

    $('#cancel_button_from_url').click(function () {
        $('#add_file_from_url_form').hide();
    });
});
