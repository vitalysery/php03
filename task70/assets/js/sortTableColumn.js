$('th').click(function() {
    var table = $(this).parents('table').eq(0);
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
    this.asc = !this.asc;

    var icon_block = $(this).children('.sort_icon').text();
    var icon_block_text = (icon_block == 'A-Z') ? 'Z-A' : 'A-Z';

    $(this).children('.sort_icon').text(icon_block_text);
    $('span.sort_icon').not($(this).find('span.sort_icon')).html('');

    if (!this.asc) {
        rows = rows.reverse();
    }

    for (var i = 0; i < rows.length; i++) {
        table.append(rows[i]);
    }
});

function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index);
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB);
    }
}

function getCellValue(row, index){
    return $(row).children('td').eq(index).html();
}