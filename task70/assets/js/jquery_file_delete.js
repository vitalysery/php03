$(document).ready(function() {
    var myFunction = function(e) {
        // Создание объекта FormData
        var fd = new FormData();

        // Чтение буферного значения, которое сожержит [ID] пользователя
        var uid = $('#uid').value;
        var sha1 = this.getAttribute("data-sha1");

        // Добавление поля, которое содержит хеш имени файла
        fd.append("sha1", sha1);

        // Добавление поля, которое содержит идентификатор пользователя
        fd.append("uid", uid);

        if (fd) {
            $.ajax({
                url: 'xhr_file_delete.php',
                type: 'POST',
                data: fd,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (resp) {
                    // Отображение сообщения для пользователя на экран
                    if(resp.message != '') {
                        if(resp.message == null || resp.messaage == 'undefined') {
                            $('#notification-wrap').html('<div class="row"> <div class="col-xs-12"> <div class="alert alert-danger"> <strong>Ошибка во время удаления файла.</strong> </div> </div> </div>');
                        } else {
                            $('#notification-wrap').html('<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>' + resp.message + '</strong> </div> </div> </div>');
                        }

                        this.closest('tr').remove();

                        // Обновляем информацию о файловой системе и пользователях в соответствии с данными, полученными в ответ
                        $('#sum_of_user_sizes').html(resp.sum_of_user_sizes);
                        $('#user_storage_limit').html(resp.user_storage_limit);
                        $('#count_users').html(resp.count_users);
                        $('#count_files').html(resp.count_files);
                        $('#sum_of_sizes').html(resp.sum_of_sizes);
                        $('#avg_files_per_all_users').html(resp.avg_files_per_all_users);
                    }
                }
            });
        }
    }
});