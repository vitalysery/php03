<?php

// Функция для поиска комбинаций всех элементов массива
// @param $array

function combine_arrays($input_array, $i) {
    if (!isset($input_array[$i])) {
        return array();
    }
    if ($i == count($input_array) - 1) {
        return $input_array[$i];
    }

    // get combinations from subsequent arrays
    $tmp = combine_arrays($input_array, $i + 1);

    $result_array = array();

    // concat each array from tmp with each element from $arrays[$i]
    foreach ($input_array[$i] as $v) {
        foreach ($tmp as $t) {
            if(is_array($t)) {
                $result_array[] = array_merge(array($v), $t);
            } else {
                $result_array[] = array($v, $t);
            }
        }
    }

    return $result_array;
}


$input_array = array
 (
  array (
         array ('name' => 'Яблоко', 'price' => 10),
         array ('name' => 'Груша',  'price' => 20),
         array ('name' => 'Слива',  'price' => 30)
        ),
  array (
         array ('name' => 'Хлеб',  'price' => 1),
         array ('name' => 'Батон', 'price' => 2),
         array ('name' => 'Булка', 'price' => 3)
        ),
  array (
         array ('name' => 'Минеральная вода',       'price' => 100),
         array ('name' => 'Сок искусственный',      'price' => 200),
         array ('name' => 'Сок натуральный',        'price' => 300),
         array ('name' => 'Напиток энергетический', 'price' => 900)
        )		
 );


$result = combine_arrays($input_array, 0);



//Вывод отсортированного массива на экран
echo '<pre>';
foreach ($result as $value) {
    echo '['. $value['name'] . ']';
    $price = $value['price'];
    foreach ($value as $v) {
        if(is_array($v)) {
            echo '[' . $v['name'] . ']';    
            $price += $v['price'];
        }
    }
    echo ' = ' . $price . '<br>';
}