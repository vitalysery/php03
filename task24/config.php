<?php

//Конфигурационный файл
$users = array(
	'vitaly' => array(
		'login' => 'vitaly',
		'password' => 'test',
		'extensions' => array(
			'txt' => 5000000,
			'jpg' => 4000000
		),
		'home_dir' => './users/vitaly',
		'available_disk_space_bytes' => 2000000
	)
);