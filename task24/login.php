<?php

session_start();
if(!empty($_SESSION['login_user'])) {
	header('Location: index.php');
}

require_once("config.php");

// Если поля логин и пароль не пустые
if(!empty($_POST['username']) && !empty($_POST['password'])) {
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	// Если введенные логин и пароль совпадают со значениями в конфигурационном файле
	// Перенаправляем пользователя на главную страницу системы
	if ($username == $users[$username]['login'] && $password == $users[$username]['password']) {
		$_SESSION['login_user'] = $username;
		header('Location: index.php');

	} else {
		echo "Неверный логин или пароль";
	}
}


//Вывод формы логина на экран
print '<!doctype html>

<html lang="ru">
	<head>
		<meta charset="utf-8">
 			<title>Страница входа в систему</title>  
		</head>
	<body>
	<div>
</div>
  <div>
  	<form method="post" action="'.basename(__FILE__).'">
  	<label for="username">Имя пользователя</label>
  	<input type="text" name="username" id="username"><br>
  	<label for="passwrord">Пароль</label>
  	<input type="password" name="password" id="password"><br>
	<input type="submit" value="Войти в систему">
  	</form>
  </div>
</body>
</html>';