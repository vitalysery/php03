<?php
session_start();

// Перенаправление на страницу логина если сессии не существует
if(empty($_SESSION['login_user'])) {
	header('Location: login.php');
} else {
	require_once('./config.php');
	$show_content = true;
	$usr = $_SESSION['login_user'];
}

// Создание домашней директории пользователя, если она не существуюет
if (!file_exists($users[$usr]['home_dir'] . "/")) {
    mkdir($users[$usr]['home_dir'] . "/", 0777);
    echo "Домашний каталог был создан.";
}


// Загрузка файла в домашнюю директорию
if(isset($_GET['file_upload'])) {
	if($_GET['file_upload'] == 1) {		
		$target_dir = $users[$usr]['home_dir'] . "/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

        if(is_uploaded_file($_FILES["fileToUpload"]["tmp_name"]))
        {
			// Определение имени и расширения файла
			$name = $_FILES["fileToUpload"]["name"];
			$ext = end((explode(".", $name)));
			
			// Поиск совпадения расширения с допустимыми в конфигурационном файле
			$extension = array_key_exists ($ext, $users[$usr]['extensions']);
			
			// Если расширение найдено
			if($extension) {
				if ($handle = opendir($users[$usr]['home_dir'])) {
    				while (false !== ($entry = readdir($handle))) {
        				// Определяем размер всех файлов с таким расширением
        				if ($entry != "." && $entry != "..") {
            				$fullFileSizeOfExtension = 0;
            				if($extension == (end((explode(".", $entry))))) {
            					$fullFileSizeOfExtension += filesize($users[$usr]['home_dir'] . "/" . $entry);
            				}
            				
        				}
    				}
    				closedir($handle);
				}
					

				// Записываем файл в домашнюю директорию	
				if($fullFileSizeOfExtension <= array_search($extension, $users[$usr]['extensions'])) {
					move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir . $_FILES["fileToUpload"]["name"]);
            		echo "Файл загружен";	
				}
				else {
					echo "Превышен лимит по размеру файлов с таким расширением.";
				}
			} else {
				echo("Расширение файла не поддерживается");	
			}

            
        } else {
            echo("Ошибка во время загрузки файла");
        }
	}
}

// Удаление файла из домашней директории
if(isset($_GET['remove_file'])) {
		$target_dir = $users[$usr]['home_dir'] . "/";
		$fileToRemove = $_GET['remove_file'];
		
        if(unlink($target_dir . $fileToRemove))
        {
            echo "Файл удален";
        } else {
            echo("Ошибка во время удаления файла");
        }
}



// Вывод списка файлов на экран
if($show_content) {
	print '<!doctype html>

	<html lang="ru">
		<head>
			<meta charset="utf-8">
 				<title>Login page</title>  
			</head>
		<body>
		<div>';
			print '<a href="./upload_file.php">Загрузить файл</a><a style="margin-left: 50px;" href="./logout.php">Выйти</a>';
			print '</div>  
			<h3>Пользовательские файлы</h3>
			<p>Каталог ' . $users[$usr]['home_dir'] . '</p>
			<table>
				<thead>
					<tr>
						<td>Размер</td>
						<td>Имя файла</td>
						<td>Скачивание</td>
						<td>Удаление</td>
					</tr>
				</thead><tbody>
			';
			

			// Получение списка файлов из домашней директории
			if ($handle = opendir($users[$usr]['home_dir'])) {
    			while (false !== ($entry = readdir($handle))) {
        			if ($entry != "." && $entry != "..") {
            			echo "<tr>
							<td style='padding-right: 30px;'>" . filesize($users[$usr]['home_dir'] . "/" . $entry)." bytes
							</td>
							<td>". $entry . "</td>
							<td><a href='" . $users[$usr]['home_dir'] . '/' . $entry . "' download>Скачать</a></td>
							<td><a href='./?remove_file=" . $entry . "'>Удалить</a></td>
            			</tr>";
        			}
    			}
    			closedir($handle);
			}

		print '</tbody></table></body>
	</html>';	
}

