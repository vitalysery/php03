<?php

require_once('./vendor/autoload.php');
require_once('./db_config.php');

error_reporting(0);

function autoload($className)
{
    require_once('class/' . $className . '.class.php');
}

spl_autoload_register('autoload');

if(!empty($_REQUEST['sha1'])) {

    $filename_sha1 = $_REQUEST['sha1'];

    // Создаем объект класса Database
    $database = Database::getInstance();

    // Создаем объект класса User
    $user = new User();

    // Читаем информацию о пользователе из базы
    // и сохраняем ее в массив
    $database->query('SELECT `u_id`, `u_login`, `u_password`, `u_email`, `u_name`, `u_storage_limit`, `u_home_dir_name`, `u_storage_filecount` FROM user WHERE `u_id` = :uid');
    $database->bind(':uid', $_REQUEST['uid']);
    $userDataFromDB = $database->single();

    // Получаем название домашнего каталога пользователя
    $target_dir = $userDataFromDB['u_home_dir_name'];

    // Получаем значение маскимально допустимого размера всех файлов пользователя
    $user_storage_limit = $userDataFromDB['u_storage_limit'];

    // Создаем объект класса Filesystem,
    // отвечающего за работу с файловой системой
    // Передаем в конструктор название домашней директории пользователя
    $filesystem = new Filesystem($target_dir);

    if ($filesystem->removeFile($filename_sha1)) {
        $database->query('DELETE FROM `files` WHERE `f_name_sha1` = :f_name_sha1 AND `u_id` = :uid');
        $database->bind(':f_name_sha1', $filename_sha1);
        $database->bind(':uid', $_REQUEST['uid']);
        $database->execute();
        $responseMessage = "Файл удален";
    } else {
        $responseMessage = "Ошибка во время удаления файла";
    }
}


// Вычисляем общее число пользователей в системе
$database->query('SELECT COUNT(`u_id`) as `all_users` FROM `user`');
$countUsers = $database->single();

// Вычисляем общий размер всех файлов в системе
$database->query('SELECT SUM(`f_size`) as `sum_of_sizes` FROM `files`');
$countFilesSize = $database->single();

// Вычисляем общее количество всех файлов в системе
$database->query('SELECT COUNT(`f_id`) as `all_files_count` FROM `files`');
$countFiles = $database->single();

// Вычисляем общий размер всех файлов пользователя в системе
$database->query('SELECT SUM(`f_size`) as `sum_of_user_sizes` FROM `files` WHERE `u_id` = ' . $userDataFromDB['u_id']);
$countUserFilesSize = $database->single();

// Вычисляем средний размер файла у пользователя
$database->query('SELECT AVG(`f_size`) as avrg FROM `files` WHERE `u_id` = :u_id');
$database->bind(':u_id', $userDataFromDB['u_id']);
$avgFiles = $database->single();

$response['user_storage_limit'] = $filesystem->bytesToSize($user_storage_limit);
$response['count_users'] = $countUsers['all_users'];
$response['sum_of_sizes'] = $filesystem->bytesToSize($countFilesSize['sum_of_sizes']);
$response['sum_of_user_sizes'] = $filesystem->bytesToSize($countUserFilesSize['sum_of_user_sizes']);
$response['count_files'] = $countFiles['all_files_count'];
$response['avg_files'] = $filesystem->bytesToSize($avgFiles['avrg']);
$response['avg_files_per_all_users'] = $filesystem->bytesToSize($countFilesSize['sum_of_sizes'] / $countUsers['all_users']);




$response['message'] = $responseMessage;

echo json_encode($response);