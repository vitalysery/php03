<?php
require_once('./vendor/autoload.php');
require_once('./db_config.php');

error_reporting(0);

function autoload($className)
{
    require_once('class/' . $className . '.class.php');
}

spl_autoload_register('autoload');

$status_for_response = 0;

if (!empty($_REQUEST['uid'])) {
    // Создаем объект класса Dataabase
    $database = Database::getInstance();

    // Читаем информацию о пользователе из базы
    // и сохраняем ее в массив
    $database->query('SELECT `u_id`, `u_login`, `u_password`, `u_email`, `u_name`, `u_storage_limit`, `u_home_dir_name`, `u_storage_filecount` FROM user WHERE `u_id` = :uid');
    $database->bind(':uid', $_REQUEST['uid']);
    $userDataFromDB = $database->single();

    //$userDataFromDB = $database->single();

// Получаем название домашнего каталога пользователя
    $target_dir = $userDataFromDB['u_home_dir_name'];
    $full_path = 'users/' . $target_dir . '/';

// Чтение логина из базы данных
    $parameters['username'] = $userDataFromDB['u_name'];

    $parameters['u_id'] = $userDataFromDB['u_id'];

// Получаем значение маскимально допустимого размера всех файлов пользователя
    $user_storage_limit = $userDataFromDB['u_storage_limit'];

// Создаем объект класса Filesystem,
// отвечающего за работу с файловой системой
// Передаем в конструктор название домашней директории пользователя
    $filesystem = new Filesystem($target_dir);

    // Читаем из базы допустимые расширения файлов ->
// для пользователя и общий размер файлов для каждого допустимого расширения
    $database->query('SELECT `extensions`.`ext_name`, `m2m_users_extensions`.`ext_limit`
FROM `user`
JOIN `m2m_users_extensions` USING(`u_id`)
JOIN `extensions` USING(`ext_id`)
WHERE `u_id` = :uid');
    $database->bind(':uid', $_REQUEST['uid']);
    $userExtensions = $database->resultset();

    foreach ($userExtensions as $item) {
        $extensions['name'][] = $item['ext_name'];
        $extensions['size'][] = $item['ext_limit'];
    }


    if (!empty($_FILES['file_to_upload']['name'])) {

        // Объединяем домашний каталог и имя файла в путь
        $target_file = $target_dir . basename($_FILES["file_to_upload"]["name"]);

        if (is_uploaded_file($_FILES["file_to_upload"]["tmp_name"])) {
            $filename = $_FILES["file_to_upload"]["name"];

            // Получаем расширение файла
            $file_extension = end((explode(".", $filename)));

            // Сравниваем расширение файла с допустимыми для этого пользователя
            $extension_availability = array_search($file_extension, $extensions['name']);

            // Получаем размер всех файлов с таким расширением в каталоге пользователя
            $files = $filesystem->getDirsize($full_path, $file_extension);

            // Получаем общий размер всех файлов в директории пользователя
            $all_dir_size = $filesystem->getDirsize($full_path);

            // Получаем допустимый размер всех файлов
            // в директории пользователя из конфигурационного файла
            $all_files_available_size = $userDataFromDB['u_storage_limit'];

            // Если допустимо расширение загружаемого файла
            if ($extension_availability !== false) {

                // Если сумма размера загружаемого файла со значением
                // общего допустимого размера директории
                // не превысит лимит после загрузки файла
                if (($all_dir_size['size'] + $_FILES["file_to_upload"]["size"]) <= $all_files_available_size) {

                    // Если сумма размера загружаемого файла со значением
                    // общего допустимого размера всех файлов с таким расширением
                    // не превысит лимит после загрузки файла
                    if (($files['size'] + $_FILES["file_to_upload"]["size"]) <= $extensions['size'][$extension_availability]) {

                        // Получаем значение хеша имени файла
                        $filename_sha1 = sha1($_FILES["file_to_upload"]["name"]);

                        // Вставляем в БД информацию о файле
                        $database->query('INSERT INTO files (`u_id`, `f_name`, `f_name_sha1`, `f_size`) VALUES (:u_id, :f_name, :f_sha1, :f_size)');
                        $database->bind(':u_id', $userDataFromDB['u_id']);
                        $database->bind(':f_name', $_FILES["file_to_upload"]["name"]);
                        $database->bind(':f_sha1', $filename_sha1);
                        $database->bind(':f_size', $_FILES["file_to_upload"]["size"]);
                        $database->execute();
                        // Загружаем файл в каталог
                        move_uploaded_file($_FILES["file_to_upload"]["tmp_name"], $full_path . $filename_sha1);
                        $responseMessage = 'Файл ' . $_FILES["file_to_upload"]["name"] . ' загружен';
                        $status_for_response = 1;

                        $last_inserted_id = $database->lastInsertId();

                        $database->query('SELECT `f_timestamp` FROM `files` WHERE `f_id` = :last_inserted_id');
                        $database->bind(':last_inserted_id', $last_inserted_id);
                        $file_info = $database->single();
                    } else {

                        // используем переменную $notification для передачи сообщений в шаблон Twig
                        $responseMessage = 'Превышен размер загружаемых файлов с расширением [ ' . $file_extension . ' ].';
                    }
                } else {
                    $responseMessage = 'Превышен лимит на размер всех файлов. Файл ' . $_FILES["file_to_upload"]["name"] . ' не загружен';
                }
            } else {
                $responseMessage = 'Расширение файла [ .' . $file_extension . ' ] не поддерживается.';
            }
        } else {
            $responseMessage = "Ошибка во время загрузки файла.";
        }
    }

} else {
    $responseMessage = '[ERROR] User not identified';
}


// Вычисляем общее число пользователей в системе
$database->query('SELECT COUNT(`u_id`) as `all_users` FROM `user`');
$countUsers = $database->single();

// Вычисляем общий размер всех файлов в системе
$database->query('SELECT SUM(`f_size`) as `sum_of_sizes` FROM `files`');
$countFilesSize = $database->single();

// Вычисляем общее количество всех файлов в системе
$database->query('SELECT COUNT(`f_id`) as `all_files_count` FROM `files`');
$countFiles = $database->single();

// Вычисляем общий размер всех файлов пользователя в системе
$database->query('SELECT SUM(`f_size`) as `sum_of_user_sizes` FROM `files` WHERE `u_id` = ' . $userDataFromDB['u_id']);
$countUserFilesSize = $database->single();

// Вычисляем средний размер файла у пользователя
$database->query('SELECT AVG(`f_size`) as avrg FROM `files` WHERE `u_id` = :u_id');
$database->bind(':u_id', $userDataFromDB['u_id']);
$avgFiles = $database->single();

$response['user_storage_limit'] = $filesystem->bytesToSize($user_storage_limit);
$response['count_users'] = $countUsers['all_users'];
$response['sum_of_sizes'] = $filesystem->bytesToSize($countFilesSize['sum_of_sizes']);
$response['sum_of_user_sizes'] = $filesystem->bytesToSize($countUserFilesSize['sum_of_user_sizes']);
$response['count_files'] = $countFiles['all_files_count'];
$response['avg_files'] = $filesystem->bytesToSize($avgFiles['avrg']);
$response['avg_files_per_all_users'] = $filesystem->bytesToSize($countFilesSize['sum_of_sizes'] / $countUsers['all_users']);

$response['message'] = $responseMessage;
$response['status'] = $status_for_response;
$response['user_homedir'] = $target_dir;
$response['filename'] = $_FILES["file_to_upload"]["name"];
$response['filesize'] = $filesystem->bytesToSize($_FILES["file_to_upload"]["size"]);
$response['timestamp'] = $file_info['f_timestamp'];
$response['sha1'] = $filename_sha1;


//$response[] = array('status' => $status_for_response);
//$response[] = array('status' => $status_for_response);

/*
$response[] = array(
    'message' => $responseMessage,
    'status' => $status_for_response,
    'filename' => $_FILES["file_to_upload"]["name"],
    'filesize' => $filesystem->bytesToSize($_FILES["file_to_upload"]["size"])
);*/

echo json_encode($response);