var asc1 = 1, asc3 = 1;

function sortTableColumn(tbody, col, asc, arrow) {
    var rows = tbody.rows, rlen = rows.length, arr = new Array(), i, j, cells, clen;

    // Заполнение массива значениями из таблицы
    for (i = 0; i < rlen; i++) {
        cells = rows[i].cells;
        clen = cells.length;
        arr[i] = new Array();
        for (j = 0; j < clen; j++) {
            arr[i][j] = cells[j].innerHTML;
        }
    }

    // Сортировка массива по номеру колонки и порядку сортировки
    // sort the array by the specified column number (col) and order (asc)
    arr.sort(function (a, b) {
        return (a[col] == b[col]) ? 0 : ((a[col] > b[col]) ? asc : -1 * asc);
    });

    // Замена текущих рядов на содержащиеся в отсортированном массиве
    for (i = 0; i < rlen; i++) {
        rows[i].innerHTML = "<td>" + arr[i].join("</td><td>") + "</td>";
    }

    // Отображение нужного положения индикатора сортировки
    var arrow_content = document.getElementById(arrow);
    arrow_content.innerHTML = (arrow_content.innerHTML == '↓') ? arrow_content.innerHTML = '↑' : arrow_content.innerHTML = '↓';
}