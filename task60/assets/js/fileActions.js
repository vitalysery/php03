window.onload = function() {
    var myFunction = function(e) {

        // Создание объекта FormData
        var fd = new FormData();

        // Чтение буферного значения, которое сожержит [ID] пользователя
        var uid = document.querySelector('#uid').value;


        // Добавление поля, которое содержит хеш имени файла
        fd.append("sha1", this.getAttribute("data-sha1"));

        // Добавление поля, которое содержит идентификатор пользователя
        fd.append("uid", uid);


        // Создание объекта XMLHttpRequest
        var xhr = new XMLHttpRequest();

        // Открытие асинхронного соединения
        // xhr_file_delete.php удаляет файл из каталога пользователя
        xhr.open('POST', 'xhr_file_delete.php', true);

        xhr.onload = function() {

            // Проверка статуса ответа
            if (this.status == 200) {

                // Парсинг json файла, который "прилетел" в ответ
                var resp = JSON.parse(this.response);

                // Если в ответе содержится сообщение
                if(resp.message != '') {
                    // Помещаем сообщение в нужный элемент
                    document.querySelector('#notification-wrap').innerHTML = '<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>' + resp.message + '</strong> </div> </div> </div>';
                }


                // Обновляем информацию о файловой системе и пользователях в соответствии с данными, полученными в ответ
                document.getElementById('sum_of_user_sizes').innerHTML = resp.sum_of_user_sizes;
                document.getElementById('user_storage_limit').innerHTML = resp.user_storage_limit;
                document.getElementById('count_users').innerHTML = resp.count_users;
                document.getElementById('count_files').innerHTML = resp.count_files;
                document.getElementById('sum_of_sizes').innerHTML = resp.sum_of_sizes;
                document.getElementById('avg_files_per_all_users').innerHTML = resp.avg_files_per_all_users;
            };
        };

        // Пересылка запроса
        xhr.send(fd);

        // Ряд таблицы
        var row = this.parentNode.parentNode;

        // Удаляем ряд таблицы, в котором отображался удаленный файл
        row.parentNode.removeChild(row);
    };


    // Добавление обработчика события на клик по кнопке, по нажатию на которую должна происходить загрузка файла
    document.querySelector('#button_file_upload').addEventListener('click', function(e) {

        // Чтение значения поля, которое содержит файл
        var file = document.querySelector('#file_to_upload').files[0];

        // Создание объекта FormData
        var fd = new FormData();

        fd.append("file_to_upload", file);


        // Чтение буферного значения, которое сожержит [ID] пользователя
        var uid = document.querySelector('#uid').value;

        fd.append("uid", uid);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'xhr_file_upload.php', true);


        // Во время загрузки файла...
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                // Вычисление числа процентов, на которое загружен файл
                var percentComplete = (e.loaded / e.total) * 100;

                // Помещаем значение в progressbar
                document.querySelector('#progressBar').value = Math.round(percentComplete);
            }
        };

        xhr.onload = function() {
            if (this.status == 200) {
                var resp = JSON.parse(this.response);

                // Сброс значения progressbar
                document.querySelector('#progressBar').value = 0;

                // Отображение сообщения для пользователя на экран
                if(resp.message != '') {
                    if(resp.message == null || resp.messaage == 'undefined') {
                        document.querySelector('#notification-wrap').innerHTML = '<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>Ошибка во время загрузки файла.</strong> </div> </div> </div>';
                    } else {
                        document.querySelector('#notification-wrap').innerHTML = '<div class="row"> <div class="col-xs-12"> <div class="alert alert-success"> <strong>' + resp.message + '</strong> </div> </div> </div>';
                    }

                }

                // Если ответ получен...
                if(resp.status == 1) {

                    // Находим таблицу с информацией о файлах
                    var table = document.getElementById("filelist");

                    // Добавляем новый ряд в конец таблицы
                    var row = table.insertRow(-1);

                    // Добавляем новые ячейки
                    var cell_for_filename = row.insertCell(0);
                    var cell_for_filesize = row.insertCell(1);
                    var cell_for_file_upload_date = row.insertCell(2);
                    var cell_for_control_buttons = row.insertCell(3);

                    // Помещаем данные в ячейки
                    cell_for_filename.innerHTML = '<a href="./users/' + resp.user_homedir + '/' + resp.filename + '" download>' + resp.filename + '</a>';
                    cell_for_filesize.innerHTML = resp.filesize;
                    cell_for_file_upload_date.innerHTML = resp.timestamp;
                    cell_for_control_buttons.innerHTML = '<a href="./users/' + resp.user_homedir + '/' + resp.filename + '" download class="btn btn-default">Скачать</a> <a href="#" data-sha1="' + resp.sha1 + '" class="btn btn-danger delete-file">Удалить</a>';

                    // Добавляем обработчик события на кнопки удаления файла
                    var classname = document.getElementsByClassName("delete-file");
                    for (var i = 0; i < classname.length; i++) {
                        classname[i].addEventListener('click', myFunction, false);
                    }


                    // Обновляем информацию о файловой системе и пользователях в соответствии с данными, полученными в ответ
                    document.getElementById('sum_of_user_sizes').innerHTML = resp.sum_of_user_sizes;
                    document.getElementById('user_storage_limit').innerHTML = resp.user_storage_limit;
                    document.getElementById('count_users').innerHTML = resp.count_users;
                    document.getElementById('count_files').innerHTML = resp.count_files;
                    document.getElementById('sum_of_sizes').innerHTML = resp.sum_of_sizes;
                    document.getElementById('avg_files_per_all_users').innerHTML = resp.avg_files_per_all_users;
                }
            };
        };
        xhr.send(fd);
    }, false);

    // Добавляем обработчик события на кнопки удаления файла
    var classname = document.getElementsByClassName("delete-file");
    for (var i = 0; i < classname.length; i++) {
        classname[i].addEventListener('click', myFunction, false);
    }
};