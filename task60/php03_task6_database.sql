-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2016 at 04:55 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php03`
--

-- --------------------------------------------------------

--
-- Table structure for table `extensions`
--

CREATE TABLE `extensions` (
  `ext_id` int(10) UNSIGNED NOT NULL,
  `ext_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extensions`
--

INSERT INTO `extensions` (`ext_id`, `ext_name`) VALUES
(1, 'txt'),
(2, 'jpg');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `f_id` int(10) UNSIGNED NOT NULL,
  `u_id` int(10) UNSIGNED NOT NULL,
  `f_name` varchar(1000) NOT NULL,
  `f_size` int(10) UNSIGNED NOT NULL,
  `f_name_sha1` char(40) NOT NULL,
  `f_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`f_id`, `u_id`, `f_name`, `f_size`, `f_name_sha1`, `f_timestamp`) VALUES
(71, 1, 'fuga3.jpg', 44176, '45db9f819e832642d3568742c1c5e4d52aafc194', '2016-04-29 14:03:56'),
(73, 1, 'kley_.jpg', 53202, 'fa4771808f31106dae35831a794f41eb3cd67286', '2016-04-29 14:32:04'),
(74, 1, 'position7.jpg', 143983, '815d03fef1f78c5c5b5addf1625e15490366d43d', '2016-04-29 14:32:21'),
(76, 1, '02-171 05.jpg', 42537, 'a689ddd18027fcf5978551fdb4035519a31cbf0d', '2016-04-29 14:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `m2m_users_extensions`
--

CREATE TABLE `m2m_users_extensions` (
  `u_id` int(10) UNSIGNED NOT NULL,
  `ext_id` int(10) UNSIGNED NOT NULL,
  `ext_limit` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m2m_users_extensions`
--

INSERT INTO `m2m_users_extensions` (`u_id`, `ext_id`, `ext_limit`) VALUES
(1, 1, 3000000),
(1, 2, 2000000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(10) UNSIGNED NOT NULL,
  `u_login` varchar(100) NOT NULL,
  `u_password` varchar(40) NOT NULL,
  `u_email` varchar(100) NOT NULL,
  `u_name` varchar(100) NOT NULL,
  `u_secure_token` char(40) NOT NULL,
  `u_storage_limit` int(10) UNSIGNED NOT NULL,
  `u_home_dir_name` varchar(50) DEFAULT NULL,
  `u_storage_filecount` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `u_login`, `u_password`, `u_email`, `u_name`, `u_secure_token`, `u_storage_limit`, `u_home_dir_name`, `u_storage_filecount`) VALUES
(1, 'vitaly', '1234', 'vitaly.sery@gmail.com', 'Vitaly Sery', '4bf931175e09b74cae7051a5c9ec4e6ce0a19b41', 5000000, 'vitaly', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `extensions`
--
ALTER TABLE `extensions`
  ADD PRIMARY KEY (`ext_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`f_id`),
  ADD KEY `FK_files_user` (`u_id`);

--
-- Indexes for table `m2m_users_extensions`
--
ALTER TABLE `m2m_users_extensions`
  ADD PRIMARY KEY (`u_id`,`ext_id`),
  ADD KEY `FK_m2m_users_extensions_extension_name` (`ext_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `UQ_login` (`u_login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `extensions`
--
ALTER TABLE `extensions`
  MODIFY `ext_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `f_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `FK_files_user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `m2m_users_extensions`
--
ALTER TABLE `m2m_users_extensions`
  ADD CONSTRAINT `FK_m2m_users_extensions_extension_name` FOREIGN KEY (`ext_id`) REFERENCES `extensions` (`ext_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_m2m_users_extensions_user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
