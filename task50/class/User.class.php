<?php

class User
{
    // Имя пользователя
    private $login;

    // Пароль
    private $password;

    // Сеттер имени пользователя
    public function setUsername($username)
    {
        $this->login = $username;
    }

    // Сеттер пароля пользователя
    public function setPassword($password)
    {
        $this->password = $password;
    }

    // Геттер имени пользователя
    public function getUsername()
    {
        return $this->login;
    }

    // Геттер пароля пользователя
    public function getUserPassword()
    {
        return $this->password;
    }

}