<?php

class Database
{
    static $instance;

    // Инициализируем значения из конфиг. файла
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;
    private $stmt;


    private $dbh;
    private $error;

    private $query_counter;
    private $exec_time;

    final private function __construct()
    {
        // Устанавливаем параметры соединения
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

        // Установка параметров соединения
        $options = array(
            // Устанавливаем постоянное соединение
            PDO::ATTR_PERSISTENT => true,

            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        // Создание объекта PDO
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        // "Ловим" возможные ошибки
        catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    // Синглтон
    public static function getInstance() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function query($query){
        $this->stmt = $this->dbh->prepare($query);
    }

    // $param - плейсхолдер, который используем в запросе
    // $value - значение плейсхолдера
    // $type - тип данных плейсхолдера
    public function bind($param, $value, $type = null){
        // Если тип не указан, используем switch для его назначения
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    // Метод для выполнения запроса
    public function execute(){
        return $this->stmt->execute();
    }

    // Возвращает массив записей
    public function resultset(){
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // Возвращает одну запись
    public function single(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    // Возвращает количество рядов, "затронутых" в запросе
    public function rowCount(){
        return $this->stmt->rowCount();
    }

    // Метод возвращает значение последнего добавленного id в виде строки
    public function lastInsertId(){
        return $this->dbh->lastInsertId();
    }

    // Метод возвращает значение времени выполнения запроса
    public function showExecutionTime() {
        return $this->exec_time;
    }

    // Метод возвращает количество запросов
    public function showQueryCount() {
        return $this->query_counter;
    }

    // Закрываем доступ к методу для клонирования объекта
    final private function __clone() {}
}