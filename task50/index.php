<?php
session_start();
error_reporting(0);

require_once('./vendor/autoload.php');
require_once('./db_config.php');

function autoload($className)
{
    require_once('class/' . $className . '.class.php');
}

spl_autoload_register('autoload');

// Разлогин пользователя
// Уничтожаем сессию
if (isset($_GET['logout'])) {
    if (isset($_GET['logout']) == '1') {
        $_SESSION['login_user'] = '';
        session_destroy();
        $template = 'login';
    }
}

// Константа для расширения шаблона
const TEMPLATE_EXTENSION = '.twig';


// Перенаправление на страницу логина если сессии не существует
if (empty($_SESSION['login_user'])) {
    $template = 'login';
} else {
    $template = 'index';
}

// Инициализируем массив параметров для передачи его в шаблонизатор Twig
$parameters = array();

// Инициализируем шаблонизатор Twig
$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

// Создаем объект класса Dataabase
$database = Database::getInstance();

// Создаем объект класса User
$user = new User();

// Пользователь пытается залогиниться
if (isset($_GET['login'])) {
    if ($_GET['login'] == 'try') {

        // Проверяем, что имя пользователя не пустое значение
        if (!empty($_POST['username'])) {
            $user->setUsername($_POST['username']);
        }

        // Проверяем, что пароль пользователя также не пустое значение
        if (!empty($_POST['password'])) {
            $user->setPassword($_POST['password']);
        }

        // Если данные совпадают с данными пользователя в базе данных,
        // то инициалищируем сессию и перенаправляем пользователя на домашнюю страницу
        // Если данные не совпадают с данными в базе, возвращаем пользователя на страницу авторизации
        $database->query("SELECT `u_login`, `u_password` FROM `user` WHERE `u_login` = :u_login AND `u_password` = :u_password");
        $database->bind(':u_login', $user->getUsername());
        $database->bind(':u_password', $user->getUserPassword());

        if ($row = $database->single()) {
            $_SESSION['login_user'] = $user->getUsername();
            $template = 'index';
        } else {
            $template = 'login';
            $notification = 'Неверный логин или пароль';
        }
    }
}

// Читаем информацию о пользователе из базы
// и сохраняем ее в массив
$database->query('SELECT `u_id`, `u_login`, `u_password`, `u_email`, `u_name`, `u_remember_me`, `u_last_login_time`, `u_storage_limit`, `u_home_dir_name`, `u_storage_filecount` FROM user WHERE `u_login` = :u_login');
$database->bind(':u_login', $_SESSION['login_user']);

$userDataFromDB = $database->single();

// Получаем название домашнего каталога пользователя
$target_dir = $userDataFromDB['u_home_dir_name'];
$full_path = 'users/' . $target_dir . '/';

// Чтение логина из базы данных
$parameters['username'] = $userDataFromDB['u_name'];

// Получаем значение маскимально допустимого размера всех файлов пользователя
$user_storage_limit = $userDataFromDB['u_storage_limit'];

// Создаем объект класса Filesystem,
// отвечающего за работу с файловой системой
// Передаем в конструктор название домашней директории пользователя
$filesystem = new Filesystem($target_dir);

// Удаление файла из домашнего каталога
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'remove_file') {
        if ($filesystem->removeFile($_GET['filename'])) {
            $database->query('DELETE FROM files WHERE `f_name_sha1` = :f_name_sha1');
            $database->bind(':f_name_sha1', $_GET['filename']);
            $database->execute();
            $notification = "Файл удален";
        } else {
            $notification = "Ошибка во время удаления файла";
        }
    }
}

// Читаем из базы допустимые расширения файлов ->
// для пользователя и общий размер файлов для каждого допустимого расширения
$database->query('SELECT `extensions`.`ext_name`, `m2m_users_extensions`.`ext_limit`
FROM `user`
JOIN `m2m_users_extensions` USING(`u_id`)
JOIN `extensions` USING(`ext_id`)
WHERE `u_login` = :u_login');
$database->bind(':u_login', $_SESSION['login_user']);
$userExtensions = $database->resultset();

foreach ($userExtensions as $item) {
    $extensions['name'][] = $item['ext_name'];
    $extensions['size'][] = $item['ext_limit'];
    $extensions_[] = array('name' => $item['ext_name'], 'size' => $filesystem->bytesToSize($item['ext_limit']));
}

$parameters['ext_name'] = $extensions_;

// Загрузка файла в домашний каталог пользователя
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'upload_file') {

        // Объединяем домашний каталог и имя файла в путь
        $target_file = $target_dir . basename($_FILES["file_to_upload"]["name"]);

        if (is_uploaded_file($_FILES["file_to_upload"]["tmp_name"])) {
            $filename = $_FILES["file_to_upload"]["name"];

            // Получаем расширение файла
            $file_extension = end((explode(".", $filename)));

            // Сравниваем расширение файла с допустимыми для этого пользователя
            $extension_availability = array_search($file_extension, $extensions['name']);

            // Получаем размер всех файлов с таким расширением в каталоге пользователя
            $files = $filesystem->getDirsize($full_path, $file_extension);

            // Получаем общий размер всех файлов в директории пользователя
            $all_dir_size = $filesystem->getDirsize($full_path);

            // Получаем допустимый размер всех файлов
            // в директории пользователя из конфигурационного файла
            $all_files_available_size = $userDataFromDB['u_storage_limit'];

            // Если допустимо расширение загружаемого файла
            if ($extension_availability !== false) {

                // Если сумма размера загружаемого файла со значением
                // общего допустимого размера директории
                // не превысит лимит после загрузки файла
                if (($all_dir_size['size'] + $_FILES["file_to_upload"]["size"]) <= $all_files_available_size) {

                    // Если сумма размера загружаемого файла со значением
                    // общего допустимого размера всех файлов с таким расширением
                    // не превысит лимит после загрузки файла
                    if (($files['size'] + $_FILES["file_to_upload"]["size"]) <= $extensions['size'][$extension_availability]) {

                        // Получаем значение хеша имени файла
                        $filename_sha1 = sha1($_FILES["file_to_upload"]["name"]);

                        // Вставляем в БД информацию о файле
                        $database->query('INSERT INTO files (`u_id`, `f_name`, `f_name_sha1`, `f_size`) VALUES (:u_id, :f_name, :f_sha1, :f_size)');
                        $database->bind(':u_id', $userDataFromDB['u_id']);
                        $database->bind(':f_name', $_FILES["file_to_upload"]["name"]);
                        $database->bind(':f_sha1', $filename_sha1);
                        $database->bind(':f_size', $_FILES["file_to_upload"]["size"]);
                        $database->execute();

                        // Загружаем файл в каталог
                        move_uploaded_file($_FILES["file_to_upload"]["tmp_name"], $full_path . $filename_sha1);
                        $notification = 'Файл ' . $_FILES["file_to_upload"]["name"] . ' загружен';
                    } else {

                        // используем переменную $notification для передачи сообщений в шаблон Twig
                        $notification = 'Превышен размер загружаемых файлов с расширением [ ' . $file_extension . ' ].';
                    }
                } else {
                    $notification = 'Превышен лимит на размер всех файлов. Файл ' . $_FILES["file_to_upload"]["name"] . ' не загружен';
                }
            } else {
                $notification = 'Расширение файла [ .' . $file_extension . ' ] не поддерживается.';
            }
        } else {
            $notification = "Ошибка во время загрузки файла.";
        }
    }

    // Если загружаем файл через URL
    if ($_GET['action'] == 'get_file_from_url') {
        $file_url = $_POST['file_url'];
        $save_type = $_POST['save_type'];

        if ($save_type == 'external_storage') {
            $filesystem->fileDownloadDirectly($file_url);
        } else {
            $filesystem->fileSaveIntoHomeDir($file_url);
        }
    }
}

// Используем имя файла шаблона страницы эквивалентное значению $_GET['page']
if (isset($_GET['page'])) {
    $template = $_GET['page'];
}

// Если пользователь залогинился, то проверяем на существование его домашний каталог
// Создаем домашний каталог, если он не существует
// Выводим список файлов на экран
if (!empty($_SESSION['login_user'])) {
    $filesystem->checkUserHomeDirectory();

    $database->query('SELECT `f_name`, `f_name_sha1`, `f_size`, `f_timestamp` FROM `files` WHERE `u_id` = :u_id');
    $database->bind(':u_id', $userDataFromDB['u_id']);
    $files_list = $database->resultset();

    foreach ($files_list as $item) {
        $parameters['files_list'][] = array('name' => $item['f_name'], 'f_name_sha1' => $item['f_name_sha1'], 'timestamp' => $item['f_timestamp'], 'size_to_display' => $filesystem->bytesToSize($item['f_size']));
    }

    //$parameters['files_list'] = $files_list;
    $parameters['user_home_dir'] = $target_dir;
}

// Передаем массив, содержащий общее количество файлов в директории и общих размер этих файлов в Twig
$parameters['directory_info'] = $filesystem->getDirsize($full_path);



// Вычисляем общее число пользователей в системе
$database->query('SELECT COUNT(`u_id`) as `all_users` FROM `user`');
$countUsers = $database->single();

// Вычисляем общий размер всех файлов в системе
$database->query('SELECT SUM(`f_size`) as `sum_of_sizes` FROM `files`');
$countFilesSize = $database->single();



// Вычисляем общее количество всех файлов в системе
$database->query('SELECT COUNT(`f_id`) as `all_files_count` FROM `files`');
$countFiles = $database->single();

$parameters['user_storage_limit'] = $filesystem->bytesToSize($user_storage_limit);

if($_SESSION['login_user'] != '') {
    // Вычисляем общий размер всех файлов пользователя в системе
    $database->query('SELECT SUM(`f_size`) as `sum_of_user_sizes` FROM `files` WHERE `u_id` = ' . $userDataFromDB['u_id']);
    $countUserFilesSize = $database->single();
}

// Вычисляем средний размер файла у пользователя
$database->query('SELECT AVG(`f_size`) as avrg FROM `files` WHERE `u_id` = :u_id');
$database->bind(':u_id', $userDataFromDB['u_id']);
$avgFiles = $database->single();

// Передаем полученные данные в шаблонизатор
$parameters['count_users'] = $countUsers['all_users'];
$parameters['sum_of_sizes'] = $filesystem->bytesToSize($countFilesSize['sum_of_sizes']);
$parameters['sum_of_user_sizes'] = $filesystem->bytesToSize($countUserFilesSize['sum_of_user_sizes']);
$parameters['count_files'] = $countFiles['all_files_count'];
$parameters['avg_files'] = $filesystem->bytesToSize($avgFiles['avrg']);
$parameters['avg_files_per_all_users'] = $filesystem->bytesToSize($countFilesSize['sum_of_sizes'] / $countUsers['all_users']);

// Если сообщение не пустое, передаем его на вывод в Twig
if (!empty($notification)) {
    $parameters['notification'] = $notification;
}

// Выводим текущий шаблон на экран
echo $twig->render($template . TEMPLATE_EXTENSION, $parameters);