SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `extension_limit` CASCADE
;

DROP TABLE IF EXISTS `extension_name` CASCADE
;

DROP TABLE IF EXISTS `files` CASCADE
;

DROP TABLE IF EXISTS `m2m_users_extensions` CASCADE
;

DROP TABLE IF EXISTS `user` CASCADE
;

CREATE TABLE `extension_limit`
(
	`limit_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`limit_value` INT NOT NULL,
	`ext_id` INT NOT NULL,
	CONSTRAINT `PK_Table1` PRIMARY KEY (`limit_id`)
)
;

CREATE TABLE `extension_name`
(
	`ext_id` INT NOT NULL AUTO_INCREMENT ,
	`ext_name` VARCHAR(50) NOT NULL,
	CONSTRAINT `PK_file_extension` PRIMARY KEY (`ext_id`)
)
;

CREATE TABLE `files`
(
	`f_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`u_id` INT UNSIGNED NOT NULL,
	`f_name` VARCHAR(1000) NOT NULL,
	`f_size` INT UNSIGNED NOT NULL,
	`f_name_sha1` VARCHAR(40) NOT NULL,
	CONSTRAINT `PK_Table1` PRIMARY KEY (`f_id`)
)
;

CREATE TABLE `m2m_users_extensions`
(
	`u_id` INT UNSIGNED NOT NULL,
	`ext_id` INT NOT NULL,
	CONSTRAINT `PK_m2m_users_extensions` PRIMARY KEY (`u_id`,`ext_id`)
)
;

CREATE TABLE `user`
(
	`u_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`u_login` VARCHAR(100) NOT NULL,
	`u_password` VARCHAR(40) NOT NULL,
	`u_email` VARCHAR(100) NOT NULL,
	`u_name` VARCHAR(100) NOT NULL,
	`u_remember_me` TINYINT UNSIGNED NOT NULL,
	`u_last_login_time` DOUBLE UNSIGNED NOT NULL,
	`u_storage_limit` INT NOT NULL,
	`u_home_dir_name` VARCHAR(50),
	`u_storage_filecount` INT UNSIGNED,
	CONSTRAINT `PK_users` PRIMARY KEY (`u_id`)
)
;

ALTER TABLE `user` 
 ADD CONSTRAINT `UQ_login` UNIQUE (`u_login`)
;

ALTER TABLE `extension_limit` 
 ADD CONSTRAINT `FK_extension_limit_extension_name`
	FOREIGN KEY (`ext_id`) REFERENCES `extension_name` (`ext_id`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `files` 
 ADD CONSTRAINT `FK_files_user`
	FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `m2m_users_extensions` 
 ADD CONSTRAINT `FK_m2m_users_extensions_extension_name`
	FOREIGN KEY (`ext_id`) REFERENCES `extension_name` (`ext_id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `m2m_users_extensions` 
 ADD CONSTRAINT `FK_m2m_users_extensions_user`
	FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE Cascade ON UPDATE Cascade
;

SET FOREIGN_KEY_CHECKS=1
