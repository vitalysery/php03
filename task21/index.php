<?php

// Функция для сортировки массива по возрастанию
// Массив сортируется в том случае, 
// если все значения массива одного типа

function sort_array_asc(&$array) { 
	foreach($array as $value) {
		if(!is_array($value)) {
			if(is_numeric($value)) {
				$typeCheck[] = 'numeric'; 
			} elseif (is_string($value)) {
				$typeCheck[] = 'string';
			} else {
				$typeCheck[] = 'other type';
			}
		}
	}
	
	if(isset($typeCheck)) {
		if(count(array_unique($typeCheck)) === 1) {
			sort($array, SORT_NATURAL | SORT_FLAG_CASE);		
		}
	}

	// Проход по каждому элементу массива
	foreach($array as &$value) { 
		if(is_array($value)) {
			sort_array_asc($value); 
		}
	}
}

//Данные для сортировки
$input_data = array
 (
  array('ZZZ', 'BBB', 'CCC'), 
  array
  (
   array (5, 2, 7), 
   array ('C', 56, TRUE), 
   array
   (
    array ('C', 'V', 'lalala'), 
    array (30, 15, 45, false), 
    array (78, 34, 'F') 
   )
  ),
  array(12, 95, 1.1, 0.8, 4)
 );

sort_array_asc($input_data);


//Вывод отсортированного массива на экран
echo '<pre>';
print_r($input_data);