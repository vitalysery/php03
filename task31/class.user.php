<?php

class User
{
    // Конфигурационный файл
    private $config_file;

    // Свойство содержит имя пользователя
    private $login;

    // Пароль
    private $password;


    // Метод устанавливает конфигурационный файл с данными пользователя
    public function setConfigFile($filename)
    {
        $this->config_file = $filename;
    }

    // Проверяем, существует ли файл конфигурации
    public function checkConfigurationFile($config_file)
    {
        if (file_exists($config_file)) {
            return 1;
        } else {
            return 0;
        }
    }


    // Задаем имя пользователя
    public function setUsername($username)
    {
        $this->login = $username;
    }


    // Задаем пароль пользователя
    public function setPassword($password)
    {
        $this->password = $password;
    }


    // Метод проверяет данные пользователя
    // Если данные совпадают
    public function checkLogin()
    {
        // Если конфигурационный файл существует
        if ($this->checkConfigurationFile($this->config_file)) {
            // Загружаем xml-файл и сохраняем его в массив
            $xml = simplexml_load_file($this->config_file) or die("Ошибка! Конфигурационный файл не может быть загружен.");
            $json = json_encode($xml);
            $user_data = json_decode($json, TRUE);

            // Если данные пользователя совпадают с введенными на странице авторизации, то возвращаем true. Иначе возвращаем false
            return ($user_data['user']['login'] == $this->login && $user_data['user']['password'] == $this->password) ? true : false;
        } else {
            exit("Ошибка! Конфигурационный файл не найден.");
        }
    }


    // Геттер имени пользователя
    public function getUsername()
    {
        return $this->login;
    }

    // Выбор определенного значения из файла xml
    public function getValueFromXml($xml_file, $parameter)
    {
        $xml = simplexml_load_file($xml_file) or die("Ошибка! Конфигурационный файл не может быть загружен.");
        $json = json_encode($xml);
        $array_from_xml = json_decode($json, TRUE);
        return $array_from_xml['user'][$parameter];
    }


    // Пытается залогинить пользователя
    public function userAuthorize()
    {
        if ($this->check_configuration_file($this->config_file)) {
            $xml = simplexml_load_file('config.xml') or die("Ошибка! Конфигурационный файл не может быть загружен.");
            $json = json_encode($xml);
            $user_data = json_decode($json, TRUE);
            return ($user_data['user']['login'] == $this->login) ? true : false;
        } else {
            exit("Ошибка! Конфигурационный файл не найден.");
        }
    }

    // Проверка существования домашнего каталога пользователя
    // Если каталога не существует, создаем его
    public function checkUserHomeDirectory($username)
    {
        if (!file_exists("users/" . $username . "/")) {
            mkdir("users/" . $username . "/", 700);
            echo "Домашний каталог был создан.";
        }
    }

    // Метод для перевода размера файла
    public function bytesToSize($bytes, $precision = 2)
    {
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;

        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';

        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';

        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';

        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';

        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }

    // Метод для получения размера файлов в директории
    // Параметр $extension необязательный
    // Если задан параметр $extension, метод вычисляет размер файлов только для заданного расширения
    function getDirsize($dir, $extension = false)
    {

        if (is_file($dir)) return array('size' => filesize($dir), 'howmany' => 0, 'size_to_display' => 0);

        if ($dh = opendir($dir)) {

            // Счетчик размера файлов
            $size = 0;

            // Счетчик количества файлов
            $n = 0;

            while (($file = readdir($dh)) !== false) {
                if ($extension === false) {
                    if ($file == '.' || $file == '..') continue;
                } else {
                    if ($file == '.' || $file == '..' || end(explode(".", $file)) != $extension) continue;
                }

                $n++;
                $data = $this->getDirsize($dir . '/' . $file);
                $size += $data['size'];
                $n += $data['howmany'];
            }
            closedir($dh);

            // Возвращаем массив со значениями
            // size: Размер файлов в байтах
            // howmany: Количество файлов
            // size_to_display: Отформатированный размер файлов для отображения в шаблоне
            return array('size' => $size, 'howmany' => $n, 'size_to_display' => $this->bytesToSize($size));
        }
        return array('size' => 0, 'howmany' => 0);
    }

    // Метод возвращает массив со всеми файлами, которые содержатся в домашней дректории пользователя
    public function showFileTree()
    {
        // Инициализируем массив файлов
        $files_list = array();

        if ($handle = opendir('./users/vitaly')) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $files_list[] = array('name' => $entry, 'size' => filesize("./users/vitaly/" . $entry), 'size_to_display' => $this->bytesToSize(filesize("./users/vitaly/" . $entry)));
                }
            }
            closedir($handle);
        }
        return $files_list;
    }


    // Удаление файла из домашней директории
    public function removeFile($filename) {
        if (unlink('./users/vitaly/' . $filename)) {
            return 1;
        } else {
            return 0;
        }
    }


    // Получаем название домашней директории из конфигурационного файла
    public function getHomeDirName()
    {
        return self::getValueFromXml('config.xml', 'home_dir');
    }

    function fileSaveIntoHomeDir($url)
    {
        $local_file = 'users/vitaly/' . end((explode("/", $url)));
        $remote_file = $url;

        // Инициализируем сеанс CURL
        $ch = curl_init();

        // Открываем файл для записи
        $fp = fopen($local_file, 'w') or die("Ошибка при открытии файла!");

        // Ассоциируем CURL с загружаемым файлом
        $ch = curl_init($remote_file);

        // Даем curl файл, в который сохраняем результат операции
        curl_setopt($ch, CURLOPT_FILE, $fp);

        // Разрешаем поддержку перенаправлений
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // Выполняем операцию
        curl_exec($ch);

        // Закрываем соединение
        curl_close($ch);

        // Закрываем файл
        fclose($fp);
    }


    function fileDownloadDirectly($url)
    {
        // Инициализируем сеанс CURL
        $ch = curl_init();

        // Запрещаем проверку сертификата удаленного сервера
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        // Разрешаем поддержку перенаправлений
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // Убираем лимит на время выполнения запроса
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);

        // Устанавливаем максимальное значение для времени подключения
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);

        // Выключаем индикацию прогресса
        curl_setopt($ch, CURLOPT_NOPROGRESS, 1);

        // Выключаем вывод сообщений о производимых действиях
        curl_setopt($ch, CURLOPT_VERBOSE, 0);

        // Отключаем возвращение результата операции
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);

        // Ассоциируем CURL с url
        curl_setopt($ch, CURLOPT_URL, $url);

        // Устанавливаем скачиваемому файлу такое же имя по умолчанию, как и его исходное имя на сервере
        $filename = end((explode("/", $url)));

        // Узнаем расширение файла для последующей установки content-type
        $file_extension = end((explode(".", $filename)));

        // Устанавливаем content-type на основе полученного расширения файла.
        // Если расширение не содержится в нашей таблице, то устанавливаем content-type по умолчанию
        switch ($file_extension) {
            case 'torrent':
                $mime_type = 'application/x-bittorrent';
            case '7z':
                $mime_type = 'application/x-7z-compressed';
            case 'swf':
                $mime_type = 'application/x-shockwave-flash';
            case 'pdf':
                $mime_type = 'application/pdf';
            case 'avi':
                $mime_type = 'video/x-msvideo';
            case 'bmp':
                $mime_type = 'image/bmp';
            case 'css':
                $mime_type = 'text/css';
            case 'curl':
                $mime_type = 'text/vnd.curl';
            case 'mp4':
                $mime_type = 'video/mp4';
            case 'jpg':
                $mime_type = 'image/jpeg';
            case 'jpeg':
                $mime_type = 'image/jpeg';
            default:
                $mime_type = 'application/octet-stream';
                break;
        }

        // Команда браузеру скачивать контент, а не открывать страницу
        header('Content-type: ' . $mime_type);
        header('Content-disposition: attachment; filename=' . $filename);

        // Выполняем операцию
        curl_exec($ch);
    }

}