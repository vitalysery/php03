<?php
session_start();

// Перенаправление на страницу логина если сессии не существует
if (empty($_SESSION['login_user'])) {
    $template = 'login';
} else {
    $template = 'index';
}


// Инициализируем массив параметров для передачи его в шаблонизатор Twig
$parameters = array();

require_once('./vendor/autoload.php');
require_once('./class.user.php');

// Инициализируем шаблонизатор Twig
$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

// Константа для расширения шаблона
define('TEMPLATE_EXTENSION', '.twig');

// Создаем объект класса User
$user = new User();

// Задаем конфигурационный файл
$user->setConfigFile('config.xml');

// Получаем название домашнего каталога пользователя
$target_dir = $user->getHomeDirName();


// Удаление файла из домашнего каталога
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'remove_file') {
        if($user->removeFile($_GET['filename'])) {
            $notification = "Файл удален";
        } else {
            $notification = "Ошибка во время удаления файла";
        }
    }
}

// Забираем из конфигурационного файла допустимые расширения файлов ->
// для пользователя и размеры всех файлов для каждого допустимого расширения
$user_extensions = $user->getValueFromXml('config.xml', 'extension');
foreach ($user_extensions as $item) {
    $extensions['name'][] = $item['name'];
    $extensions['size'][] = $item['size_in_bytes'];
}


// Загрузка файла в домашний каталог пользователя
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'upload_file') {

        // Объединяем домашний каталог и имя файла в путь
        $target_file = $target_dir . basename($_FILES["file_to_upload"]["name"]);

        if (is_uploaded_file($_FILES["file_to_upload"]["tmp_name"])) {
            $filename = $_FILES["file_to_upload"]["name"];

            // Получаем расширение файла
            $file_extension = end((explode(".", $filename)));

            // Сравниваем расширение файла с допустимыми для этого пользователя
            $extension_availability = array_search($file_extension, $extensions['name']);

            // Получаем размер всех файлов с таким расширением в каталоге пользователя
            $files = $user->getDirsize($user->getHomeDirName(), $file_extension);

            // Получаем общий размер файлов в директории пользователя
            $all_dir_size = $user->getDirsize($user->getHomeDirName());

            // Получаем допустимый размер всех файлов
            // в директории пользователя из конфигурационного файла
            $all_files_available_size = $user->getValueFromXml('config.xml', 'disk_space_in_bytes');

            // Если допустимо расширение загружаемого файла
            if ($extension_availability !== false) {

                // Если сумма размера загружаемого файла со значением
                // общего допустимого размера директории
                // не превысит лимит после загрузки файла
                if (($all_dir_size['size'] + $_FILES["file_to_upload"]["size"]) <= $all_files_available_size) {


                    // Если сумма размера загружаемого файла со значением
                    // общего допустимого размера всех файлов с таким расширением
                    // не превысит лимит после загрузки файла
                    if (($files['size'] + $_FILES["file_to_upload"]["size"]) <= $extensions['size'][$extension_availability]) {

                        // Загружаем файл в каталог
                        move_uploaded_file($_FILES["file_to_upload"]["tmp_name"], $target_dir . $_FILES["file_to_upload"]["name"]);
                        $notification = 'Файл ' . $_FILES["file_to_upload"]["name"] . ' загружен';
                    } else {

                        // используем переменную $notification для передачи сообщений в шаблон Twig
                        $notification = 'Превышен размер загружаемых файлов с расширением [ ' . $file_extension . ' ].';
                    }
                } else {
                    $notification = 'Превышен лимит на размер всех файлов. Файл ' . $_FILES["file_to_upload"]["name"] . ' не загружен';
                }
            } else {
                $notification = 'Расширение файла [ .' . $file_extension . ' ] не поддерживается.';
            }
        } else {
            $notification = "Ошибка во время загрузки файла.";
        }
    }

    // Если загружаем файл через URL
    if ($_GET['action'] == 'get_file_from_url') {
        $file_url = $_POST['file_url'];
        $save_type = $_POST['save_type'];


        if ($save_type == 'external_storage') {
            $user->fileDownloadDirectly($file_url);
        } else {
            $user->fileSaveIntoHomeDir($file_url);
        }
    }
}

// Пользователь пытается залогиниться
if (isset($_GET['login'])) {
    if ($_GET['login'] == 'try') {

        // Проверяем, что имя пользователя не пустое значение
        if (!empty($_POST['username'])) {
            $user->setUsername($_POST['username']);
        }

        // Проверяем, что пароль пользователя также не пустое значение
        if (!empty($_POST['password'])) {
            $user->setPassword($_POST['password']);
        }

        // Если данные совпадают с данными в конфигурационном файле,
        // то инициалищируем сессию и перенаправляем пользователя на домашнюю страницу
        // Если данные не верны, возвращаем пользователя на страницу авторизации
        if ($user->checkLogin()) {
            $_SESSION['login_user'] = $user->getUsername();
            $template = 'index';
        } else {
            $template = 'login';
            $notification = 'Неверный логин или пароль';
        }
    }
}


// Используем имя файла шаблона страницы эквивалентное значению $_GET['page']
if (isset($_GET['page'])) {
    $template = $_GET['page'];
}

// Если пользователь залогинился, то проверяем на существование его домашний каталог
// Создаем домашний каталог, если он не существует
// Выводим список файлов на экран
if (!empty($_SESSION['login_user'])) {
    $user->checkUserHomeDirectory($_SESSION['login_user']);
    $parameters['files_list'] = $user->showFileTree();
    $parameters['user_home_dir'] = $target_dir;
}


// Разлогин пользователя
// Уничтожаем сессию
if (isset($_GET['logout'])) {
    if (isset($_GET['logout']) == '1') {
        $_SESSION['login_user'] = '';
        session_destroy();
        $template = 'login';
    }
}

// Передаем массив, содержащий общее количество файлов в директории и общих размер этих файлов в Twig
$parameters['directory_info'] = $user->getDirsize($user->getHomeDirName());


// Если сообщение не пустое, передаем его на вывод в Twig
if (!empty($notification)) {
    $parameters['notification'] = $notification;
}


// Выводим текущий шаблон
echo $twig->render($template . TEMPLATE_EXTENSION, $parameters);