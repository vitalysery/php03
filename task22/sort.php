<?php
// Запуск из командной строки
// php sort.php [ИМЯ_ИСХОДНОГО_ФАЙЛА_С_РАСШИРЕНИЕМ] [ИМЯ_ФАЙЛА_ДЛЯ_СОХРАНЕНИЯ_РЕЗУЛЬТАТА_С_РАСШИРЕНИЕМ]


$input_file = 'generated.txt';
$result_file = 'result.txt';

// Если запуск из командной строки
if (isset($argv[0]) && basename($argv[0]) == basename(__FILE__)){
    $commandline = true;
    $input_file = $argv[1];
    $result_file = $argv[2];
}



//Удаление верхней строки из файла, значение из которого является наименьшим
function deleteLineFromFile($filename) {
 	$file = file($filename);
	unset($file[0]);
	$fp = fopen($filename, "w");
	fwrite($fp, implode($file)); 
	fclose($fp);
}


// Удаление временных директории и файлов
function removeDirectory($dir) {
    if ($objs = glob($dir."/*")) {
       foreach($objs as $obj) {
         is_dir($obj) ? removeDirectory($obj) : unlink($obj);
       }
    }
    rmdir($dir);
}

// Создание директории для хранения "разбитых" файлов
if (!file_exists("blocks/")) {
    mkdir("blocks/", 0777);
}

$line_counter = 0;
$desired_line = 50;
$fname_counter = 1;


$small_files_dir = 'blocks/';

// Подсчет строк в исходном файле
$lineCount = count(file($input_file));
$fh = fopen($input_file,'r') or die($php_errormsg);

if(!is_file($result_file)) {

} else {
	unlink($result_file);	
}


while((!feof($fh))) {

		// Чтение блока значений
		for($i=$line_counter; $i <= $desired_line; $i++) {
			if ($s = fgets($fh)) {
        		if(mb_strlen($s) !== 0) {
        			$arr[] = fgets($fh);	
        		}
    		}	
		}
		
		array_multisort($arr);

		// Отсортированные значения по возрастанию заносятся в файл небольшого размера
		foreach($arr as $item) {
			if(!empty($item)) {
				file_put_contents ('./blocks/data'.$fname_counter.'.txt' , $item, FILE_APPEND);		
			}
        }

        $arr = '';
    	$line_counter = $desired_line + 1;
    	$desired_line += 50;
    	$fname_counter++;
    	$to_file = '';
}

fclose($fh) or die($php_errormsg);

$cnt = 1;
while($cnt < $fname_counter) {
	$name_arr[] = 'data' . $cnt . '.txt';
	$cnt++;
}

for($i = 0; $i <= $lineCount; $i++) {
	foreach($name_arr as $filename) {
		$fh = fopen($small_files_dir . $filename,'rb') or die($php_errormsg);
			if ($s = fgets($fh)) {
				if(mb_strlen($s) !== 0) {
        			$first_line[] = fgets($fh);	
        		}
    		}
    	fclose($fh);
	}

	$result_query = fopen($result_file,'a+') or die($php_errormsg);
	fwrite($result_query, min($first_line));

deleteLineFromFile($small_files_dir . $name_arr[array_search(min($first_line), $first_line)]);
$first_line = '';

}

removeDirectory($small_files_dir);

$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo $time;
